<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function fooditems() {
    	return $this->hasMany('App\FoodItem', 'category_id', 'id');
    }

    public function type() {
    	return $this->belongsTo('App\Type', 'id', 'type_id');
    }
}
