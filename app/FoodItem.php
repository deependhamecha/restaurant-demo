<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Config;

class FoodItem extends Model
{
    public function category() {
    	return $this->belongsTo('App\Category', 'category_id', 'id');
    }

    // public function getImageAttribute($value)
    // {
    //     return Config::get('constants.fooditem_image_dir').$value;
    // }
}
