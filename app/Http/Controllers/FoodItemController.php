<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FoodItem;
use App\Category;
use App\Type;

class FoodItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([ 'data' => FoodItem::all()], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Type::all()->pluck('name');
        $categories = Category::all()->pluck('name');
        $type_ids = Type::all()->pluck('id');
        $category_ids = Category::all()->pluck('id');

        return response()->json([   'types' => $types,
                                    'categories' => $categories,
                                    'type_ids' => $type_ids,
                                    'category_ids' => $category_ids
                                ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fooditem = new FoodItem();
        $fooditem->name = $request->name;
        $fooditem->ingredients = $request->ingredients;
        $fooditem->price = $request->price;
        // $fooditem->type_id = $request->type;
        $fooditem->category_id = $request->category;

        if($request->hasFile('file')) {
            // dd($request->file);
            $filename = time().".".$request->file->extension();
            // dd($filename);
            $isTrue = $request->file->storeAs('/public/images/fooditems', $filename);
            // dd($isTrue);
            $fooditem->image = $filename;
        }
        // return response()->json(['success' => $fooditem], 200);

        $fooditem->save();
        $allFoodItems = FoodItem::all();

        return response()->json([ 'data' => $allFoodItems ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $fooditem = FoodItem::findOrFail($id);
            $category_name = $fooditem->category()
                                      ->where('id', $fooditem->category_id)
                                      ->first(['name']);

            return response()->json( ['data' => $fooditem, 'category' => $category_name], 200 );
        } catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json([ 'data' => [] ], 204);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $fooditem = FoodItem::findOrFail($id);
            $fooditem->name = $request->name;
            $fooditem->ingredients = $request->ingredients;
            $fooditem->price = $request->price;
            // $fooditem->type_id = $request->type;
            $fooditem->category_id = $request->category;

            if($request->hasFile('file')) {
                // dd($request->file);
                $filename = time().".".$request->file->extension();
                // dd($filename);
                $isTrue = $request->file->storeAs('/public/images/fooditems', $filename);
                // dd($isTrue);
                $fooditem->image = $filename;
            }
            // return response()->json(['success' => $fooditem], 200);

            $fooditem->save();
            $allFoodItems = FoodItem::all();

            return response()->json([ 'data' => $allFoodItems ], 200);

        } catch(Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json([ 'data' => [] ], 204);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fooditem = FoodItem::find($id);
        $fooditem->delete();

        return response()->json(['success' => 1], 200);
    }
}
