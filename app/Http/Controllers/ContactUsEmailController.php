<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactUsEmail;
use Mail;

class ContactUsEmailController extends Controller
{
    public function index() {
        $contactusemails = ContactUsEmail::all();

        if( is_null($contactusemails) && empty($contactusemails)) {
            return response()->json(['data', 'Content Not Found'], 204);
        }
        return response()->json(['data' => $contactusemails], 200);
    }

    public function sendMail(Request $request) {

        Mail::raw($request->body, function($message) use ($request) {
            $message->to('restaurantabc123@gmail.com', 'Restaurant Abc')
                    ->subject('Contact Us Email');

            $message->from('restaurantabc123@gmail.com', $request->name);
        });

        $contactusemail = new ContactUsEmail();
        $contactusemail->name = $request->name;
        $contactusemail->body = $request->body;
        $contactusemail->save();

        $contactusemail = ContactUsEmail::all();
        return response()->json([ 'success'=> 1, 'data'=> $contactusemail ], 200);

    }
}
