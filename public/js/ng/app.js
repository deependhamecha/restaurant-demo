var mainApp = angular.module('mainApp', ['ngRoute']);


// Routing
mainApp.config(function($routeProvider) {

    $routeProvider
    .when('/website',{
        templateUrl : 'js/ng/website/index.html',
        controller : 'websiteController'
    })
    .when('/dashboard',{
        templateUrl : 'js/ng/dashboard/index.html',
        controller : 'dashboardController'
    });

});


// Controller Definitions
mainApp.controller('websiteController', function($scope) {

});

mainApp.service('multipartForm', ['$http', function($http) {
        this.post = function(uploadUrl, data) {
            var fd = new FormData();
            for(var key in data)
                fd.append(key, data[key]);
            $http.post(uploadUrl, fd, {
                transformRequest: angular.indentity,
                headers: { 'Content-Type': undefined }
            });
        }
}]); 

mainApp.controller('dashboardController', function($scope, multipartForm, $http) {

    $scope.tableData = [];


    $http({
      method: 'GET',
      url: '/api/fooditem',
      headers: {
        'Accept' : 'application/json'
      },
    }).then(function successCallback(response) {
        $scope.tableData = response.data.data;
      }, function errorCallback(response) {
        $scope.tableData = response.data;
    });


    // Get Types and Categories
    $scope.createFoodItem = function() {
        // console.log('createFoodItem');
        $http({
          method: 'GET',
          url: '/api/fooditem/create',
          headers: {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
          }
        }).then(function successCallback(response) {
            $scope.typeData = response.data.types;
            $scope.categoryData = response.data.categories;
            $scope.typeIds = response.data.type_ids;
            $scope.categoryIds = response.data.category_ids;
            // console.log($scope.typeData[0]);
            return true;
          }, function errorCallback(response) {
            $scope.tableData = response.data;
            return false;
        });
    }

    $scope.createFoodItem();

    // console.log($scope.typeData);
    mainApp.directive('fileInput', function($parse) {
        return {
            restrict: 'A',
            link: function(scope, elem, attrs) {
                elem.bind('change', function() {
                    $parse(attrs.fileInput).assign(scope, elem[0], files)
                    $scope.$apply()
                })
            }
        }
    });

    $scope.fileChanged = function(elem) {
        $scope.files = elem.files;
        $scope.$apply();
    }


    $scope.addFoodItem = function() {

        var fd = new FormData();
        fd.append('name', $scope.name);
        fd.append('ingredients', $scope.ingredients);
        fd.append('price', $scope.price);
        fd.append('category', $scope.category);
        angular.forEach($scope.files, function(file) {
            fd.append('file', file)
        });
        $http({
          method: 'POST',
          url: '/api/fooditem/store',
          transformRequest: angular.identity,
          headers: {
            'Accept' : 'application/json',
            'charset' : 'utf-8',
            'boundary' : Math.random().toString().substr(2),
            'Content-Type' : undefined
          },
          data: fd
        }).then(function successCallback(response) {
            console.log(response.data.data);
            // $scope.$apply(function() {
                $scope.tableData = response.data.data;
            // })
            // console.log($scope.tableData);
            $('#addModal').modal('hide');
            return true;
          }, function errorCallback(response) {
            $scope.tableData = response.data;
            return false;
        });
    }
                            // selected="{{ (categoryIds[$index] == categoryId) ? 'selected': '' }}"
    $scope.editFoodItem = function(id) {
        console.log('coming to edit '+id);
        $http({
          method: 'GET',
          url: '/api/fooditem/edit/'+id+'',
          headers: {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json'
          }
        }).then(function successCallback(response) {
            $scope.name = response.data.data.name;
            $scope.ingredients = response.data.data.ingredients;
            $scope.price = response.data.data.price;
            $scope.categoryId = response.data.data.category_id;
            $scope.category = response.data.category;
            // console.log($scope.typeData[0]);
            return true;
          }, function errorCallback(response) {
            $scope.tableData = response.data;
            return false;
        });
    }

    $scope.getEmails = function() {

        $http({
          method: 'GET',
          url: '/api/contactusemails',
          headers: {
            'Accept' : 'application/json'
          },
        }).then(function successCallback(response) {
            $scope.tableData = response.data.data;
          }, function errorCallback(response) {
            $scope.tableData = response.data;
        });

    }

});

