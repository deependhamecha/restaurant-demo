<!DOCTYPE html>
<html>
<head>
    <title>Abc Restaurant</title>
</head>
<body>
    <header>
        @yield('header')
    </header>
    <section class="container">
        @yield('section')
    </section>
    <footer>
        @yield('footer')
    </footer>

</body>
</html>