<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::group(['prefix' => 'api'], function() {
// 	Route::resource('fooditem', 'FoodItemController');
// });

Route::get('fooditem', 'FoodItemController@index');
Route::get('fooditem/create', 'FoodItemController@create');
Route::post('fooditem/store', 'FoodItemController@store');
Route::post('fooditem/edit/{$id}', 'FoodItemController@edit');
Route::post('fooditem/update/{$id}', 'FoodItemController@update');

Route::get('contactusemails', 'ContactUsEmailController@index');
Route::post('sendemail', 'ContactUsEmailController@sendMail');

